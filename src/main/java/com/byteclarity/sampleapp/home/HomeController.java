package com.byteclarity.sampleapp.home;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;

import com.byteclarity.sampleapp.model.Employee;

@Controller
public class HomeController {
	
	@Autowired
	WebApplicationContext context;
	
	
	@RequestMapping(value = "/", method = RequestMethod.GET )
	@ResponseBody
	public Employee index() {
		
		Employee e = new Employee();
		e.id = 1;
		e.name = "murdock";
		return e;
	}
}
